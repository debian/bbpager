//  blackboxstyle.hh for bbpager - an pager for Blackbox.
//  workspaces.
//
//  Copyright (c) 1998-2000 by John Kennis, jkennis@chello.nl
//
//  this program is free software; you can redistribute it and/or modify
//  it under the terms of the gnu general public license as published by
//  the free software foundation; either version 2 of the license, or
//  (at your option) any later version.
//
//  this program is distributed in the hope that it will be useful,
//  but without any warranty; without even the implied warranty of
//  merchantability or fitness for a particular purpose.  see the
//  gnu general public license for more details.
//
//  you should have received a copy of the gnu general public license
//  along with this program; if not, write to the free software
//  foundation, inc., 675 mass ave, cambridge, ma 02139, usa.
//
// (see the included file copying / gpl-2.0)
//

#ifndef __BLACKBOXSTYLE_HH
#define __BLACKBOXSTYLE_HH

/* bbpager.frame */
#define BB_FRAME "toolbar"
#define BB_C_FRAME "Toolbar"
/* bbpager.desktop */
#define BB_LABEL "toolbar.label"
#define BB_C_LABEL "Toolbar.Label"
/* bbpager.focused.window */
#define BB_WINDOW_FOCUS "window.label.focus"
#define BB_C_WINDOW_FOCUS "Window.Label.Focus"
/* bbpager.window */
#define BB_WINDOW_UNFOCUS "window.label.unfocus"
#define BB_C_WINDOW_UNFOCUS "Window.Label.Unfocus"

#endif /* __BLACKBOXSTYLE_HH */
